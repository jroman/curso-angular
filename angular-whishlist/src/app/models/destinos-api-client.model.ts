import { DestinoViaje } from './destino-viaje.model';
import {Subject, BehaviorSubject} from 'rxjs';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';
@Injectable()
export class DestinosApiClient{
  destinos: DestinoViaje[] = [];
  constructor(private store: Store<AppState>,
              @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
              private http: HttpClient){
  }
  add(d: DestinoViaje) {
    // agregando llamada a api
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, {headers});
    this.http.request(req).subscribe(
      (data: HttpResponse<{}>) => {
        if (data.status === 200){
          this.store.dispatch(new NuevoDestinoAction(d));
          const myDb = db;
          myDb.destinos.add(d);
          console.log('todos los destinos de la db!');
          myDb.destinos.toArray().then(destinos => console.log(destinos));
        }
      }
    );
    // fin llamada api
  }

  elegir(d: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  getById(id: string){
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }

  getALl(): DestinoViaje[] {
    return this.destinos;
  }
}

