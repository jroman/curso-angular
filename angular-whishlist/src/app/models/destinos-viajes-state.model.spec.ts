import { DestinoViaje } from './destino-viaje.model';
import { BorrarDestinoAction, DestinosViajesState, ElegidoFavoritoAction, initializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction, redurcerDestinosViajes, VoteDownAction, VoteUpAction } from "./destinos-viajes-state.model"

describe('reducerDestinosViajes', () => {
  it('should reduce init data', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });

  it('should reduce new item added', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });

  it('should reduce  item elegido favorito', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].isSelected());
  });

  it('should reduce item deleted', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: BorrarDestinoAction = new BorrarDestinoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(0);
  });

  it('should reduce vote up', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: VoteUpAction = new VoteUpAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].votes).toEqual(1);
  });
  it('should reduce vote down', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: VoteDownAction = new VoteDownAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = redurcerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].votes).toEqual(1);
  });

});
